﻿using AtomReader.ViewModels;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AtomReader
{
	public partial class AtomReaderPage : ContentPage
	{
		private AtomReaderPageViewModel ViewModel { get; set; }
			

		public AtomReaderPage()
		{
			InitializeComponent();
            this.Title = "AtomReader";

			this.ViewModel = new AtomReaderPageViewModel();
			this.BindingContext = this.ViewModel;
		}

        protected override async void OnAppearing()
        {
            base.OnAppearing();


            await Refresh();
        }



        private async void List_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var atom = e.SelectedItem as AtomEntry;
            if(atom != null) {
                await Navigation.PushAsync(new AtomWebViewPage(atom));
            }
        }

        public async Task Refresh()
		{
			var atom = this.ViewModel.Config.Atoms.FirstOrDefault();
			this.ViewModel.Entries = new ObservableCollection<AtomEntry>(
				await new AtomDownloader().DownloadAtomAsync(atom.URL)
				);
		}
	}
}
