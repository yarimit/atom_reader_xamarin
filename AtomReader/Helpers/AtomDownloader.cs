﻿using AtomReader.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace AtomReader
{
	public class AtomDownloader
	{
		public AtomDownloader()
		{
		}



		public async Task<IEnumerable<AtomEntry>> DownloadAtomAsync(string url)
		{
			var httpClient = new HttpClient();
			var content = await httpClient.GetStringAsync(url);

			return parseAtomEntry(content);
		}

		private IEnumerable<AtomEntry> parseAtomEntry(string content)
		{
			XDocument doc = XDocument.Parse(content);
			XNamespace nsAtom = "http://www.w3.org/2005/Atom";

			return doc.Descendants(nsAtom + "entry").Select(e => {
                return new AtomEntry() {
                    Title = e.Element(nsAtom + "title").Value,
                    Content = e.Element(nsAtom + "content").Value,
                    LinkHref = e.Element(nsAtom + "link").Attribute("href").Value
				};
			});
		}
	}
}
