﻿using AtomReader.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace AtomReader
{
    public partial class AtomWebViewPage : ContentPage
    {
        private AtomWebViewPageViewModel ViewModel { get; set; }

        public AtomWebViewPage(AtomEntry atom)
        {
            InitializeComponent();

            this.Title = atom.Title;
            this.ViewModel = new AtomWebViewPageViewModel() {
                URL = atom.LinkHref
            };
            this.BindingContext = this.ViewModel;
        }
    }
}
