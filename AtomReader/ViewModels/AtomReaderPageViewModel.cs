﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace AtomReader.ViewModels
{

	public class AtomReaderPageViewModel : Monitorable
	{
		public ConfigModel Config_;
		public ConfigModel Config
		{
			get
			{
				return Config_;
			}
			set
			{
				Config_ = value;
				NotifyPropertyChanged();
			}
		}

		public ObservableCollection<AtomEntry> Entries_;
		public ObservableCollection<AtomEntry> Entries
		{
			get
			{
				return Entries_;
			}
			set
			{
				Entries_ = value;
				NotifyPropertyChanged();
			}
		}

		public AtomReaderPageViewModel()
		{
			this.Config = new ConfigModel()
			{
				Atoms = new List<AtomConfigModel>()
				{
					new AtomConfigModel() {
						URL = "https://news.google.com/news?hl=ja&ned=us&topic=h&ie=UTF-8&output=atom&num=10"
					}
				}
			};
		}
	}
}
