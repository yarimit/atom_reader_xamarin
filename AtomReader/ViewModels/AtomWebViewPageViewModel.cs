﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtomReader.ViewModels
{
    class AtomWebViewPageViewModel : Monitorable
    {
        public string URL_;

        public string URL {
            get {
                return URL_;
            }
            set {
                URL_ = value;
                NotifyPropertyChanged();
            }
        }
    }
}
