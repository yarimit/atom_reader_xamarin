﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AtomReader.ViewModels
{
    public class AtomEntry : Monitorable
    {
        public string Title_;
        public string Title {
            get {
                return Title_;
            }
            set {
                Title_ = value;
                NotifyPropertyChanged();
            }
        }

        public string LinkHref_;
        public string LinkHref {
            get {
                return LinkHref_;
            }
            set {
                LinkHref_ = value;
                NotifyPropertyChanged();
            }
        }

        public string Content_;
        public string Content {
            get {
                return Content_;
            }
            set {
                Content_ = value;
                NotifyPropertyChanged();
            }
        }
    }
}
